self.jmhc ?= {}

safe = (id) -> id.replace /[:\.\/\[\]]/g, "_"

benchmarkId: (mode, unit, className, methodName) ->
    safe("#{className}.#{methodName}_#{mode}_#{unit}")

class jmhc.BenchmarkSet
  constructor: ->
    @paramsWithValues = {}
    @benchmarks = []
    @addBenchmark = (benchmark) ->
      @benchmarks.push benchmark
      (@paramsWithValues[param] || @paramsWithValues[param] = []).push value for param, value of benchmark.params when value not in (@paramsWithValues[param] || [])

class jmhc.Benchmarks
  clazzes: {}
  addBenchmark: (b) ->
    clazz = (@clazzes[b.className] || @clazzes[b.className] = {})
    m = (clazz[b.methodName] || clazz[b.methodName] = new jmhc.BenchmarkSet)
    m.addBenchmark b
  getClazzes: ->
    return (clazz for clazz, _ of @clazzes)
  getMethods: (className) ->
    return (methodName for methodName, _ of @clazzes[className])
  getParams: (className, methodName) ->
    return (paramName for paramName, _ of @clazzes[className][methodName].paramsWithValues)
  getParamValues: (className, methodName, paramName) ->
    return @clazzes[className][methodName].paramsWithValues[paramName]

class Chart


class Metric
  constructor: (@name, @namespace, jmhMetric) ->
    @score = jmhMetric.score
    @error = jmhMetric.scoreError
    @confidence = jmhMetric.scoreConfidence
    @percentiles = jmhMetric.scorePercentiles

jmhInfoText =
  threads: "Threads"
  forks: "Forks"
  warmupIterations: "Warmup iterations"
  warmupTime: "Warmup time"
  measurementIterations: "Measurement iterations"
  measurementTime: "Measurement time"

class jmhc.Benchmark
  constructor: (jmh, @filename, @filetime) ->
    [@methodName, className, packages...] = jmh.benchmark.split(".").reverse()
    @className = "#{packages.reverse()}.#{className}"
    @params = jmh.params
    @paramsString = "(" + ("#{name}=#{value}" for name, value of jmh.params).join(", ") + ")"
    @info = do ->
      info = {}
      for key, text of jmhInfoText
        info[key] =
          text: text
          value: jmh[key]
      info
    @mode = jmh.mode
    @unit = jmh.primaryMetric.scoreUnit
    @primary = new Metric(@name, @namespace, jmh.primaryMetric)
    @secondaries = do =>
      namespace = "#{@namespace}.#{@name}"
      # TODO: Perhaps consider unit normalization, in case of differences
      for name, jmhMetric of jmh.secondaryMetrics when jmhMetric.scoreUnit == @unit
        new Metric(name, namespace, jmhMetric)

class jmhc.UploadFile
  @id: (name, timestamp) -> safe("#{timestamp}_#{name}")
  constructor: (@id, @name, @timestamp) ->
    @benchmarks = []
