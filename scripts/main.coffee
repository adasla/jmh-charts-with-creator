angular.module('App', ['ui.jq', 'ngCookies']).controller 'AppCtrl', ($scope, $timeout, $cookieStore) ->
  @config = $cookieStore.get('config') || {showConfidence: false}
  $scope.watchedConfig = @config
  $scope.$watch 'watchedConfig', (newConfig) ->
    $cookieStore.put('config', newConfig)
  , true

  @uploaded = {}
  benchmarks = new jmhc.Benchmarks
  @benchmarks = benchmarks
  @creator = { fixed: {} }
  @fixParam = (p,v) ->
    @creator.fixed[p] = v

  @resetCharts = ->
#    for _, file of @uploaded
#      for b in file.benchmarks then charts.addBenchmark b
#    return

  @charts = []

  @createChart = ->
    combine = (acc, lls) ->
      if lls.length == 0 then acc else
        head = lls[0]
        tail = lls[1..]
        newAcc = []
        for e1 in head
          for e2 in acc
            newAcc.push(Object.assign({}, e2, e1))
        combine(newAcc, tail)
    xParam = @creator.xParam
    set = benchmarks.clazzes[@creator.clazz][@creator.method]
    bs = (b for b in set.benchmarks when b.className == @creator.clazz and b.methodName == @creator.method )
    any = bs[0]
    categories = set.paramsWithValues[xParam]
    excludedParams = (x for x, _ of @creator.fixed)
    excludedParams.push(xParam)
    seriesNames = combine([{}], (({"#{p}": v} for v in vs) for p,vs of set.paramsWithValues when p not in excludedParams))
    findValue = (name, cat) ->
      pred = (el) ->
        if el.params[xParam] isnt cat then return false
        for p,v of name
          if el.params[p] isnt v then return false
        return true
      tmp = bs.find pred
      return tmp?.primary.score
    series = ({name: ("#{p}: #{v}" for p,v of name).join(" "), data: (findValue(Object.assign({}, name, @creator.fixed), cat) for cat in categories)} for name in seriesNames)
    @charts.push ( {benchmarks: [{className: @creator.clazz, methodName:@creator.method}], unit: any.unit, id: "#{@creator.method}#{xParam}", mode:any.mode, categories: categories, series: series, xlegend: xParam, fixedParams: ("#{p}: #{v}" for p,v of @creator.fixed).join(" ") } )
    @creator = { fixed: {} }

  @showChart = (sideId) ->
    sideSelector = ".side:not(.active):has(##{sideId})"
    $side = $(sideSelector)
    $side.closest('.shape').shape('set next side', sideSelector).shape('flip over')
    return

  @render = (chart, $scope) ->
    $timeout =>
      height = Math.round($(window).height() * .67)
      sc = renderScore(chart, @config, height)
      $scope.$on '$destroy', -> sc.destroy();
      return
      return
    , 0, false
    return

  @removeFile = (file) -> 
    delete @uploaded[file.id]
    @resetCharts()
    return

  @loadFiles = (input) ->
    for file in input.files
      id = jmhc.UploadFile.id file.name, file.lastModifiedDate.getTime()
      if @uploaded[id]?
        alert "File '#{file.name}' has already been added. Ignoring..."
      else
        reader = new FileReader
        reader.readAsText file
        reader.onload = do (@uploaded, id, file, reader) -> -> 
          $scope.$apply ->
            try
              jmhBencharks = JSON.parse reader.result
            catch
              alert "File \"#{file.name}\" does not contain valid JSON. Ignoring..."
              return
            uploadFile = new jmhc.UploadFile id, file.name, file.lastModifiedDate.getTime()
            for jmh in jmhBencharks
              benchmark = new jmhc.Benchmark jmh, file.name, file.lastModifiedDate.getTime()
              benchmarks.addBenchmark benchmark
              uploadFile.benchmarks.push benchmark
            @uploaded[id] = uploadFile
            return # $apply
          return # onload
    $(input).val("")
    return # loadFile

  return # controller

angular.bootstrap document.querySelector('#App'), ['App']

jmhModes =
  thrpt: "Throughput"
  avgt: "Average time"
  sample: "Sampling time"
  ss: "Single invocation time"

sharedChartOptions = (custom = {}) ->
  formatNum = (n) ->
    switch
      when n > 1 then n.abbr(1)
      when n < 0.000000001 then n.abbr(12)
      when n < 0.000001 then n.abbr(9)
      else n.abbr(6)
  opt = $.extend true,
    credits: enabled: false
    exporting: enabled: true
    colors: [
      '#4572A7', '#AA4643', '#89A54E', '#80699B', '#3D96AE', '#DB843D', '#92A8CD', '#A47D7C', '#B5CA92',
      '#7cb5ec', '#434348', '#90ed7d', '#f7a35c', '#8085e9', '#f15c80', '#e4d354', '#8085e8', '#8d4653', '#91e8e1',
      '#2f7ed8', '#0d233a', '#8bbc21', '#910000', '#1aadce', '#492970', '#f28f43', '#77a1e5', '#c42525', '#a6c96a'
    ]
    chart:
      style: fontFamily: "Signika, serif"
    plotOptions:
      series: 
        groupPadding: 0
        stickyTracking: false
        marker: enabled: false
    tooltip:
      formatter: ->
        tooltip = "<b>#{@point.name || @series.name}</b>"
        tooltip += "<br/>Score: #{formatNum @point.y} #{@point.unit}"
        if @point.filename?
          tooltip += "<br/>File: #{@point.filename}"
        for _, info of @point.info
          tooltip += "<br/>#{info.text}: #{info.value}"
        tooltip
  , custom
  opt

renderScore = (chart, config, height) ->
  seriesName = (bench) ->
    bench.paramsString
  any = chart.benchmarks[0]
  options = sharedChartOptions
    title: text: "#{jmhModes[chart.mode]} scores (#{chart.unit})"
    subtitle: text: "#{any.className}.#{any.methodName} #{chart.fixedParams}"
    chart:
      type: 'line'
      zoomType: 'xy'
      renderTo: "#{chart.id}_scores"
      height: height
    xAxis:
      labels: enabled: true
      categories: chart.categories
      title: text: chart.xlegend
    yAxis:
      title: text: "#{chart.unit}"
      min: 0
    legend: layout: 'vertical'
    series:
        chart.series
#    drilldown:
#      series: for bench in chart.benchmarks when bench.secondaries.length
#        id: "#{bench.namespace}.#{bench.name}"
#        name: seriesName(bench)
#        data: for secondary in bench.secondaries
#          name: secondary.name
#          y: secondary.score
#          unit: bench.unit
#          dataLabels:
#            enabled: true
#            formatter: -> @point.name
  new Highcharts.Chart options
